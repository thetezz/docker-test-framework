FROM jenkinsci/jenkins:2.154
USER root
COPY plugins.txt /var/jenkins_home/plugins.txt
RUN /usr/local/bin/plugins.sh /var/jenkins_home/plugins.txt
RUN apt-get update && apt-get install -y python-pip && apt-get install sshpass && apt-get install -y vim 
RUN rm -rf /var/lib/apt/lists/*
RUN pip install ansible
RUN pip install boto3
RUN git config --global user.email "needssomething@aol.com"
RUN git config --global user.name "needsSomting Heretoo"

COPY JOB_CONFIGS/ /usr/share/jenkins/ref/jobs/
COPY JENKINS_CONFIGS/ /var/jenkins_home/
COPY REPOS/ /tmp/
