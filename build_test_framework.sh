#!/bin/bash

echo "cloning libraries, inventory, and test-framework..."
git clone https://some_uzr:PRbeFU3YDds8obvPAjg8@gitlab.com/thetezz/erplibs.git REPOS/erplibs
git clone https://some_uzr:sDvWi9gGfctznWbN-RLn@gitlab.com/thetezz/demoplaybook.git REPOS/demoplaybook
git clone https://gitlab.com/thetezz/test-framework.git REPOS/test-framework

docker build -t test-framework .
